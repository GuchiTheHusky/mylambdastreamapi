package functionalProgramming;

import java.util.*;

import static java.util.stream.Collectors.*;

public class ExampleOneInt {
    public static void main(String[] args) {
        // Список з 10-ти елементів, заповнений в рандомний спосіб.
        int number;
        List<Integer> listNumbers = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            number = random.nextInt(10);
            listNumbers.add(number);
        }
        // Зводимо елементи списку в квадрат.
        List<Integer> listNumbersSecond = listNumbers.stream().map(e -> e * e).collect(toList());
        // Виводимо суму елементів списку попередньо виведених в квадрат.
        int sumNumbers = listNumbersSecond.stream().reduce(Integer::sum).get();
        // Виводимо кількість парних чисел другого списка.
        int num = (int) (listNumbersSecond.stream().filter((e) -> (e % 2) == 0).count());

        System.out.println("List of numbers #1:" + "\n" + listNumbers);
        System.out.println("List of numbers #2:" + "\n" + listNumbersSecond);
        System.out.println("The sum of the numbers in the second list:" + "\n" + sumNumbers);
        System.out.println("The number of even numbers in the second list:" + "\n" + num);

        // Тестую методи, що виводять суму парних тп непарних чисел масива.
        int[]arrayTest= {2, 2 , 5, 7};
        System.out.println(methodEvenNumbers(arrayTest));
        System.out.println(methodNotEvenNumbers(arrayTest));
    }
    public static int methodEvenNumbers(int[]array) {
        return Arrays.stream(array).filter((e) -> (e % 2) == 0).sum();
    }
    public static int methodNotEvenNumbers (int[]array){
        return Arrays.stream(array).filter((e) -> (e % 2) != 0).sum();
    }
}


