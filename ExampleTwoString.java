package functionalProgramming;

import java.util.stream.*;
import java.util.*;
import java.util.function.*;

public class ExampleTwoString {

    public static void main(String[] args) {
        streamAPI();
    }
    public static void streamAPI() {

        String[] arraySurname = {"Johnson", "Green", "Parker", "Hill", "Young", "Jackson", "King", "Jones", "James", "Lee"};

        List<String> listSurname = new ArrayList<>(arraySurname.length);
        Collections.addAll(listSurname, arraySurname);
        Predicate <String> findJ;

        findJ = (str) -> {
            return str.charAt(0) == 'J';
        };
        Stream<String> allSurname = listSurname.stream();
        List<String> surnameJ = allSurname.filter(findJ).collect(Collectors.toList());

        System.out.println(surnameJ);
    }
}